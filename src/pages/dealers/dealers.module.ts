import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DealerPage } from './dealers';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DealerPage
  ],
  imports: [
    IonicPageModule.forChild(DealerPage),
    TranslateModule.forChild()
  ]
})
export class DealerPageModule {}
